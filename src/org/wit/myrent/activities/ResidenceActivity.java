package org.wit.myrent.activities;


import static org.wit.android.helpers.LogHelpers.info;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.wit.myrent.R;
import org.wit.myrent.app.MyRentApp;
import org.wit.myrent.models.Portfolio;
import org.wit.myrent.models.Residence;

import static org.wit.android.helpers.IntentHelper.navigateUp;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import static org.wit.android.helpers.ContactHelper.getDisplayName;
import static org.wit.android.helpers.ContactHelper.getEmail;
import static org.wit.android.helpers.IntentHelper.selectContact;
import android.content.Intent;
import static org.wit.android.helpers.IntentHelper.sendEmail;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Email;


public class ResidenceActivity extends Activity implements TextWatcher, OnCheckedChangeListener, OnClickListener, DatePickerDialog.OnDateSetListener
{
	
	private static final int REQUEST_CONTACT = 1;
	private Button   tenantButton;
	private Button   reportButton;
	private Button   emailButton;

	
  private EditText geolocation;
  private CheckBox rented;
  private Button   dateButton;
  

  private Residence residence;
  private Portfolio portfolio; 
  private TextView tweetemail;
  private static final int CONTACT_PICKER_RESULT = 1001;


  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_residence);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    
    tenantButton = (Button)   findViewById(R.id.tenant);
    geolocation = (EditText) findViewById(R.id.geolocation);
    dateButton  = (Button)   findViewById(R.id.registration_date);
    rented      = (CheckBox) findViewById(R.id.isrented);
    reportButton = (Button)   findViewById(R.id.residence_reportButton);
    tweetemail =   (EditText) findViewById(R.id.invite_email);
    emailButton = (Button)   findViewById(R.id.do_email_picker);



    tweetemail.addTextChangedListener(this);
    tenantButton.setOnClickListener(this);
    geolocation.addTextChangedListener(this);
    dateButton  .setOnClickListener(this);
    rented     .setOnCheckedChangeListener(this);
    reportButton.setOnClickListener(this);
    emailButton.setOnClickListener(this);
    tweetemail.addTextChangedListener(this);



    MyRentApp app = (MyRentApp) getApplication();
    portfolio = app.portfolio;    

    UUID resId = (UUID) getIntent().getExtras().getSerializable("RESIDENCE_ID");
    residence = portfolio.getResidence(resId);
    if (residence != null)
    {
      updateControls(residence);
    }   
  }

  public void updateControls(Residence residence)
  {
    geolocation.setText(residence.geolocation);
    rented.setChecked(residence.rented);
    tweetemail.setText(residence.email);
    dateButton.setText(residence.getDateString());

  }

  public void onPause() 
  {
    super.onPause();
    portfolio.saveResidences();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:  navigateUp(this);
                               return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCheckedChanged(CompoundButton arg0, boolean isChecked)
  {
    info(this, "rented Checked");
    residence.rented = isChecked;
  }

  @Override
  public void afterTextChanged(Editable c)
  {
    Log.i(this.getClass().getSimpleName(), "geolocation " + c.toString());
    residence.geolocation = c.toString();
  }

  @Override
  public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
  {
  }

  @Override
  public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
  {
  }

  @Override
  public void onClick(View v)
  {
    switch (v.getId())
    {
      case R.id.registration_date      : Calendar c = Calendar.getInstance();
                                         DatePickerDialog dpd = new DatePickerDialog (this, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                                         dpd.show();
      break;
                                         
      case R.id.tenant                 : selectContact(this, REQUEST_CONTACT);
      break;  
      
      case R.id.residence_reportButton : sendEmail(this, "" , getString(R.string.residence_report_subject), residence.getResidenceReport(this));                           
      break; 
      
      case R.id.do_email_picker        : Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
                                         startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);                           
      break;
    }
  }

  @Override
  public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
  {
    Date date = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
    residence.date = date;
    dateButton.setText(residence.getDateString());
  }
  
  /*@Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch (requestCode)
    {
      case REQUEST_CONTACT    : String tenantName = getDisplayName(this, data);
                                String tenantEmail = getEmail(this, data);
                                residence.tenant = tenantName;
                                residence.email = tenantEmail;
                                tenantButton.setText(tenantName);  
                                tweetemail.setText(tenantEmail);
                                if (tweetemail.length() == 0) 
                                {  
                        	        Log.v("GET_EMAIL", "Not found " + tenantEmail);
                                } 
                                else
                                {
                        	        Log.v("GET_EMAIL", "Got email: " + tenantEmail);
                                }
                                break;
                                
    }
  } */
  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (resultCode == RESULT_OK) {
          switch (requestCode) {
          case CONTACT_PICKER_RESULT:
              Cursor cursor = null;
              String email = "";
              try {
                  Uri result = data.getData();
                  Log.v("GET_EMAIL", "Got a contact result: "
                          + result.toString());

                  // get the contact id from the Uri
                  String id = result.getLastPathSegment();

                  // query for everything email
                  cursor = getContentResolver().query(Email.CONTENT_URI,
                          null, Email.CONTACT_ID + "=?", new String[] { id },
                          null);

                  int emailIdx = cursor.getColumnIndex(Email.DATA);

                  // let's just get the first email
                  if (cursor.moveToFirst()) {
                      email = cursor.getString(emailIdx);
                      Log.v("GET_EMAIL", "Got email: " + email);
                  } else {
                      Log.w("GET_EMAIL", "No results");
                  }
              } catch (Exception e) {
                  Log.e("GET_EMAIL", "Failed to get email data", e);
              } finally {
                  if (cursor != null) {
                      cursor.close();
                  }
                  EditText emailEntry = (EditText) findViewById(R.id.invite_email);
                  emailEntry.setText(email);
                  if (email.length() == 0) {
                      Toast.makeText(this, "No email found for contact.",
                              Toast.LENGTH_LONG).show();
                  }

              }

              break;
          }

      } else {
          Log.w("GET_EMAIL", "Warning: activity result not ok");
      }
  }
}
