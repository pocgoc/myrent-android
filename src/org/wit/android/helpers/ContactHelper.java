package org.wit.android.helpers;

import org.wit.myrent.R;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import android.content.ContentResolver;



public class ContactHelper {

	public static String getDisplayName(Context context, Intent data)
	{
		String contact = "unable to find contact";

		Uri contactUri = data.getData();
		String[] queryFields = new String[] { ContactsContract.Contacts.DISPLAY_NAME };

		Cursor c = context.getContentResolver().query(contactUri, queryFields, null, null, null);

		if (c.getCount() == 0)
		{
			c.close();
			return contact;
		}
		c.moveToFirst();
		contact = c.getString(0);
		c.close();

		return contact;
	}

	public static String getEmail(Context context, Intent data)
	{
		String email = "";

		Uri contactUri = data.getData();
		Log.v("GET_EMAIL", "Got a contact result: "
				+ contactUri.toString());
		ContentResolver cr = context.getContentResolver();

		Cursor cursor = cr.query(contactUri, null, null, null, null);
       try
       {
		if (cursor.getCount() > 0)
		{

			// get the contact id from the Uri
			String id = contactUri.getLastPathSegment();

			// query for everything email
			String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
			Cursor emails = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
			int emailIdx = cursor.getColumnIndex(Email.DATA);

			// let's just get the first email
			if (cursor.moveToFirst()) {
				email = cursor.getString(emailIdx);
				Log.v("GET_EMAIL", "Got email: " + email);
			} else {
				Log.w("GET_EMAIL", "No results");
			}

		}
       }
		catch (Exception e) 
		{
			Log.e("GET_EMAIL", "Failed to get email data", e);
		} 
		finally 
		{
			if (cursor != null) 
			{
				cursor.close();
			}
		}


		return email;
}
}

